//
//  JKUIPickDate.h
//  OCTest
//
//  Created by 王冲 on 2018/2/25.
//  Copyright © 2018年 希爱欧科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PassValue)(NSString *str);

@interface JKUIPickDate : UIView

+(instancetype)setDate;

-(void)passvalue:(PassValue)block;


@end
